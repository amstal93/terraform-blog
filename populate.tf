# Minimum space creation for this set of use cases. Will create the `blog19`
# space, could be changed in the resource `solidserver_ip_space` around
# line 32
#
# terraform apply -auto-approve -target=null_resource.populate
# terraform.exe import -provider=solidserver solidserver_ip_space.blog 6
#
# may 2019

resource "null_resource" "populate" {
  depends_on = [
                 "solidserver_ip_subnet.HDQ",
                 "solidserver_ip_subnet.NCE",
                 "solidserver_ip_subnet.ATL",
                 "solidserver_ip_subnet.PARDC1",
                 "solidserver_ip_subnet.PARDC2",
                 "solidserver_ip_subnet.SINDC1",
                 "solidserver_ip_subnet.NYCDC1",
                 "solidserver_ip_subnet.NYCDC2",
                 "solidserver_ip_space.blog",
                 "solidserver_ip_subnet.PAR-VPC01-a"
               ]
}

resource "solidserver_dns_zone" "blog19" {
  dnsserver = "smart.ud19"
  name      = "blog19"
  type      = "master"
  createptr = false
}

resource "solidserver_ip_space" "blog" {
  name   = "blog19"
  class_parameters {
    dns_id   = "2"
    dns_view_name = "#all"
    domain_list   = "${solidserver_dns_zone.blog19.name}"
    domain        = "${solidserver_dns_zone.blog19.name}"
    rev_dns_id    = "2"
    rev_dns_view_name = "#all"
    dns_update    = "1"
    use_ipam_name = "0"
  }

  provisioner "local-exec" {
    when    = "destroy"
    command = "ping 127.0.0.1 -n 10 > NUL"
    on_failure = "continue"
  }
  provisioner "local-exec" {
    when    = "destroy"
    command = "sleep 10"
    on_failure = "continue"
  }
}

resource "solidserver_ip_subnet" "sites" {
  space            = "${solidserver_ip_space.blog.name}"
  request_ip       = "172.21.0.0"
  size             = 20
  name             = "sites"
  terminal         = false
}

resource "solidserver_ip_subnet" "EMEA" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.sites.name}"
  size             = 22
  name             = "EMEA"
  gateway_offset   = -1
  terminal         = false
}

resource "solidserver_ip_subnet" "AMER" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.sites.name}"
  size             = 22
  name             = "AMER"
  gateway_offset   = -1
  terminal         = false
}

resource "solidserver_ip_subnet" "NCE" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.EMEA.name}"
  size             = 24
  name             = "Nice"
  gateway_offset   = -1
  terminal         = true
}

resource "solidserver_ip_subnet" "ATL" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.AMER.name}"
  size             = 24
  name             = "Atlanta"
  gateway_offset   = -1
  terminal         = true
}

resource "solidserver_ip_subnet" "servers" {
  space            = "${solidserver_ip_space.blog.name}"
  request_ip       = "172.20.0.0"
  size             = 20
  name             = "servers"
  terminal         = false
}

resource "solidserver_ip_subnet" "HDQ" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.servers.name}"
  size             = 25
  request_ip       = "172.20.15.128"
  name             = "HDQ"
  gateway_offset   = -1
  terminal         = true
}

resource "solidserver_ip_subnet" "PAR" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.servers.name}"
  size             = 22
  name             = "PAR"
  gateway_offset   = -1
  terminal         = false
}

resource "solidserver_ip_subnet" "PARDC2" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.PAR.name}"
  size             = 24
  name             = "PAR_DC2"
  gateway_offset   = -1
  terminal         = true
}

resource "solidserver_ip_subnet" "PARDC1" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.PAR.name}"
  size             = 24
  name             = "PAR_DC1"
  gateway_offset   = -1
  terminal         = true
}

resource "solidserver_ip_subnet" "NYC" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.servers.name}"
  size             = 23
  name             = "NYC"
  gateway_offset   = -1
  terminal         = false
}

resource "solidserver_ip_subnet" "NYCDC1" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.NYC.name}"
  size             = 26
  name             = "NYC_DC1"
  gateway_offset   = -1
  terminal         = true
}

resource "solidserver_ip_subnet" "NYCDC2" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.NYC.name}"
  size             = 26
  name             = "NYC_DC2"
  gateway_offset   = -1
  terminal         = true
}

resource "solidserver_ip_subnet" "SIN" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.servers.name}"
  size             = 23
  name             = "SIN"
  gateway_offset   = -1
  terminal         = false
}

resource "solidserver_ip_subnet" "SINDC1" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.SIN.name}"
  size             = 26
  name             = "SIN_DC1"
  gateway_offset   = -1
  terminal         = true
}

resource "solidserver_ip_subnet" "AWS" {
  space            = "${solidserver_ip_space.blog.name}"
  request_ip       = "10.128.0.0"
  size             = 16
  name             = "AWS-EC2"
  terminal         = false
}

resource "solidserver_ip_subnet" "AWS-PAR" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.AWS.name}"
  size             = 20
  name             = "AWS-PAR"
  terminal         = false
}

resource "solidserver_ip_subnet" "AWS-PAR-VPC01" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.AWS-PAR.name}"
  size             = 24
  name             = "AWS-PAR-VPC01"
  terminal         = false
}

resource "solidserver_ip_subnet" "PAR-VPC01-a" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.AWS-PAR-VPC01.name}"
  size             = 27
  name             = "PAR-VPC01-a"
  terminal         = true
}

resource "solidserver_ip_subnet" "PAR-VPC01-b" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.AWS-PAR-VPC01.name}"
  size             = 27
  name             = "PAR-VPC01-b"
  terminal         = true
}

resource "solidserver_ip_subnet" "PAR-VPC01-c" {
  space            = "${solidserver_ip_space.blog.name}"
  block            = "${solidserver_ip_subnet.AWS-PAR-VPC01.name}"
  size             = 27
  name             = "PAR-VPC01-c"
  terminal         = true
}
