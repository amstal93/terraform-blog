# Use case 1: reserve an IP address

# terraform plan -target=null_resource.ex01
# terraform apply -auto-approve -target=null_resource.ex01
# terraform.exe destroy -auto-approve -target=solidserver_ip_address.SX01I02

# get an existing space
#data "solidserver_ip_space" "blog19" {
#  name = "blog19"
#}

# create an IP address for a future host
resource "solidserver_ip_address" "SX01I02" {
  # space   = "blog19"
  space   = "${solidserver_ip_space.blog.name}"
  # subnet  = "PAR_DC1"
  subnet  = "${solidserver_ip_subnet.PARDC1.name}"
  name    = "SX01I02"
}

output "SX01I02-ip" {
  value = "SX01I02: ${solidserver_ip_address.SX01I02.address}"
}

resource "null_resource" "ex01" {
  depends_on = [
                 "solidserver_ip_address.SX01I02"
               ]
}
