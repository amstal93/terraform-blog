# Use case 3: reserve an IP address and set DNS entries for A, PTR and CNAME

# terraform plan -target=null_resource.ex03
# terraform apply -auto-approve -target=null_resource.ex03
# terraform.exe destroy -auto-approve -target=solidserver_ip_address.SX01M04

# create an IP address for a future host linked to the existing space
resource "solidserver_ip_address" "SX01M04" {
  space   = "${solidserver_ip_space.blog.name}"
  # subnet  = "PAR_DC1"
  subnet  = "${solidserver_ip_subnet.PARDC1.name}"
  name    = "sx01m04.blog19"
  mac     = "f7:93:a1:76:da:d3"
}

resource "solidserver_dns_rr" "CNAME_WWW" {
  dnsserver    = "smart.ud19"
  name         = "www.blog19"
  type         = "CNAME"
  value        = "${solidserver_ip_address.SX01M04.name}"
}

output "SX01M04-ip" {
  value = "SX01M04: ${solidserver_ip_address.SX01M04.address}  ${solidserver_dns_rr.CNAME_WWW.name}"
}

resource "null_resource" "ex03" {
  depends_on = [
                 "solidserver_ip_address.SX01M04",
                 "solidserver_dns_rr.CNAME_WWW"
               ]
}
